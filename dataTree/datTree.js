let sharedValues = new Array();
let areaNum = 0;
// Represents the node in the tree. Will be displayed as a small circle in the browser.
// x, y --> x, y co-ordinates of the center of circle
// r    --> radius of the circle
// ctx  --> context of the canvas
// data --> data to be displayed (Only number)

var Node = function (x, y, r, ctx, data, color) {
  // left child of a node
  // this.leftNode = null;
  // right child of a node
  // this.rightNode = null;
  this.right = null;
  this.left = null;
  this.color = color;
  this.parent = null;
  this.x = x;
  this.y = y;
  // console.log(this.color);
  this.ctx = ctx;
  this.data = data;
  // this.x = x;
  // this.y = y;
  this.setCtx = function (newContext) {
    this.ctx = newContext;
  };
  getCtx = function () {
    return this.ctx;
  };

  // draw function. Responsible for drawing the node
  this.draw = function () {
    // console.log("begin draw " + data);

    this.ctx.fillStyle = this.color;
    // console.log("The fill style :", this.color);
    this.ctx.beginPath();
    this.ctx.arc(this.x, this.y, r, 0, 2 * Math.PI);
    this.ctx.fill();
    //this.ctx.fillStyle = "black";
    this.ctx.closePath();
    this.ctx.strokeStyle = "white";
    this.ctx.strokeText(this.data, this.x, this.y);
    // console.log("End draw " + data);

    // ctx.strokeStyle = "black"
  };

  // Simple getters
  this.getData = function () {
    return data;
  };
  this.getX = function () {
    return this.x;
  };
  this.getY = function () {
    return this.y;
  };
  this.getRadius = function () {
    return this.r;
  };

  // Returns coordinate for the left child
  // Go back 3 times radius in x axis and
  // go down 3 times radius in y axis
  this.leftCoordinate = function () {
    return { cx: this.x - 3 * r, cy: this.y + 3 * r };
  };
  // Same concept as above but for right child
  this.rightCoordinate = function () {
    return { cx: this.x + 3 * r, cy: this.y + 3 * r };
  };
};

this.setX = function (theX) {
  this.x = theX;
};

this.setY = function (theY) {
  this.y = theY;
};

// Draws a line from one circle(node) to another circle (node)
var Line = function () {
  // Takes
  // x,y      - starting x,y coordinate
  // toX, toY - ending x,y coordinate
  this.draw = function (x, y, toX, toY, r, ctx) {
    ctx.strokeStyle = "black";
    var moveToX = x;
    var moveToY = y + r;
    var lineToX = toX;
    var lineToY = toY - r;
    ctx.beginPath();
    ctx.moveTo(moveToX, moveToY);
    ctx.lineTo(lineToX, lineToY);
    ctx.stroke();
  };
};

// Represents the btree logic
var BTree = function () {
  appendCanvas();
  var c = document.getElementById(`my-canvas-${areaNum}`);
  this.ctx = c.getContext("2d");
  var line = new Line();
  this.root = null;
  this.values = new Array();

  var self = this;
  this.updateContext = function () {
    temp = document.getElementById(`my-canvas-${areaNum}`);
    // console.log(temp);
    this.ctx = temp.getContext("2d");
  };

  this.getCtx = function () {
    return this.ctx;
  };

  // Getter for root
  this.getRoot = function () {
    return this.root;
  };

  // Adds element to the tree
  this.add = function (data) {
    // If root exists, then recursively find the place to add the new node
    if (this.root) {
      //here we have to add the new node in the array
      var newAddNodeA = this.recursiveAddNode(
        this.root,
        null,
        null,
        data,
        "red"
      );
      console.log("New added node", newAddNodeA.newValue);
      this.insertCorrect(newAddNodeA.newValue);
    } else {
      // console.log("Root");
      // If not, the add the element as a root
      this.root = this.addAndDisplayNode(2000, 20, 15, this.ctx, data, "black");
      return this.root;
    }
  };

  // Recurively traverse the tree and find the place to add the node
  this.recursiveAddNode = function (
    nodeRAdd,
    prevNode,
    coordinateCallback,
    data,
    color
  ) {
    // console.log("Recursif add of ", data);
    if (!nodeRAdd) {
      // console.log("ENter here", prevNode);
      // This is either nodeRAdd.leftCoordinate or nodeRAdd.rightCoordinate
      var xy = coordinateCallback;
      var newNode = this.addAndDisplayNode(
        xy.cx,
        xy.cy,
        15,
        this.ctx,
        data,
        color
      );
      line.draw(prevNode.getX(), prevNode.getY(), xy.cx, xy.cy, 15, this.ctx);
      newNode.parent = prevNode;
      // console.log("recent display of ", newNode);
      // var x = this.root;
      // console.log("current root: ", x);
      // console.log("recent display of ", newNode);
      return { newNode: newNode, newValue: newNode };
    } else {
      var tempN;
      if (data <= nodeRAdd.getData()) {
        tempN = this.recursiveAddNode(
          nodeRAdd.left,
          nodeRAdd,
          nodeRAdd.leftCoordinate(),
          data,
          color
        );
        nodeRAdd.left = tempN.newNode;

        // return nodeRAdd;

        // console.log(`Add of ${data} in left add recursive`);
      } else {
        tempN = this.recursiveAddNode(
          nodeRAdd.right,
          nodeRAdd,
          nodeRAdd.rightCoordinate(),
          data,
          color
        );
        nodeRAdd.right = tempN.newNode;

        // return nodeRAdd;

        // console.log(`Add of ${data} in right add recursive`);
      }
      return { newNode: nodeRAdd, newValue: tempN.newValue };
    }
  };

  // Adds the node to the tree and calls the draw function
  this.addAndDisplayNode = function (x, y, r, ctx, data, color) {
    var nodeAD = new Node(x, y, r, ctx, data, color);
    nodeAD.draw();
    nodeAD.right = null;
    nodeAD.left = null;
    // console.log("add of nodeAD ", nodeAD);
    sharedValues.push(nodeAD);
    return nodeAD;
  };

  this.showAndCorrectMe = function () {
    var tempTree = new BTree();

    tempTree.updateContext();
    this.ctx = tempTree.ctx;
    tempTree.root = tempTree.addAndDisplayNode(
      2000,
      20,
      15,
      tempTree.ctx,
      this.root.data,
      "black"
    );
    showCorrect(this.root.right, tempTree);
    showCorrect(this.root.left, tempTree);
    this.root = tempTree.root;
    // console.log("temp root", tempTree.root);
    // console.log("temp root right", tempTree.root.right);
    // console.log("temp root last", tempTree.root?.right?.left);
    // tempTree.show();
    // this.show();
  };

  this.show = function () {
    if (this.root) {
      // console.log(sharedValues);
      appendCanvas();
      this.updateContext();

      this.recursiveAddNodeOnView(this.root);
    }
  };

  this.recursiveAddNodeOnView = function (nodeAV) {
    // console.log("Recursif show of", nodeAV);
    if (nodeAV) {
      //if the nodeAV exists, we have to draw it
      nodeAV.setCtx(this.ctx);
      nodeAV.draw();
      if (nodeAV.left) {
        //if the left nodeAV exists, we have to link anticipate by draw the line which will link him with the parent nodeAV
        //and then draw it
        line.draw(
          nodeAV.getX(),
          nodeAV.getY(),
          nodeAV.left.getX(),
          nodeAV.left.getY(),
          15,
          this.ctx
        );
        this.recursiveAddNodeOnView(nodeAV.left);
      }

      //Same for the right  nodeAV

      if (nodeAV.right) {
        line.draw(
          nodeAV.getX(),
          nodeAV.getY(),
          nodeAV.right.getX(),
          nodeAV.right.getY(),
          15,
          this.ctx
        );

        this.recursiveAddNodeOnView(nodeAV.right);
      }
    }
  };

  this.leftRotate = function (nodeX) {
    // console.log("left rotation of ", nodeX);
    // console.log("Right node of nodeX", nodeX.right);

    var nodeY = nodeX.right;
    nodeX.right = nodeY.left;
    if (nodeY.left) {
      nodeY.left.parent = nodeX;
    }
    nodeY.parent = nodeX.parent;
    if (nodeX.parent == null) {
      this.root = nodeY;
      // console.log("nodeY", nodeY);
    } else {
      if (nodeX.parent.left == nodeX) {
        nodeX.parent.left = nodeY;
      } else {
        nodeX.parent.right = nodeY;
      }
    }
    nodeY.left = nodeX;
    nodeX.parent = nodeY;
    // this.show();
  };

  this.rightRotate = function (xNodeRR) {
    // console.log("right rotation of ", xNodeRR);
    var yNodeRR = xNodeRR.left;
    xNodeRR.left = yNodeRR.right;
    if (yNodeRR.right) {
      yNodeRR.right.parent = xNodeRR;
    }
    yNodeRR.parent = xNodeRR.parent;
    if (xNodeRR.parent == null) {
      this.root = yNodeRR;
    } else if (xNodeRR.parent.left == xNodeRR) {
      xNodeRR.parent.left = yNodeRR;
    } else {
      xNodeRR.parent.right = yNodeRR;
    }
    yNodeRR.right = xNodeRR;
    xNodeRR.parent = yNodeRR;
    // this.show();
  };

  this.insertCorrect = function (node) {
    // console.log("this.root", this.root);
    // console.log("XNode before rotation *****", node.parent.parent);
    console.log("Add correct of", node);
    // var grandP = node.parent.parent;
    // console.log("Add correct red", node.parent.color == "red");

    while (node?.parent?.color == "red") {
      // console.log("Add correct red", node.parent.color == "red");
      // console.log("Shared ", sharedValues);
      // return;
      console.log("eneter");
      var nodeUncle;
      if (node.parent == node.parent.parent.left) {
        console.log("uncle is right");
        nodeUncle = node?.parent?.parent?.right;
        if (nodeUncle && nodeUncle.color == "red") {
          node.parent.color = "black";
          nodeUncle.color = "black";
          node.parent.parent.color = "red";
          node = node.parent.parent;
        } else {
          if (node == node.parent.right) {
            node = node.parent;
            this.leftRotate(node);
          }
          node.parent.color = "black";
          node.parent.parent.color = "red";
          this.rightRotate(node.parent.parent);
        }
      } else {
        console.log("Uncle  is left");
        nodeUncle = node?.parent?.parent?.left;
        if (nodeUncle && nodeUncle.color == "red") {
          node.parent.color = "black";
          nodeUncle.color = "black";
          node.parent.parent.color = "red";
          node = node.parent.parent;
        } else {
          console.log("Uncle  is black");

          if (node == node.parent.left) {
            console.log("rigt rotation");
            node = node.parent;
            this.rightRotate(node);
          }
          // console.log("XNode before rotation granp", grandP);
          node.parent.color = "black";
          node.parent.parent.color = "red";
          this.leftRotate(node.parent.parent);
        }
      }
      // console.log("The node", node);
    }
    this.root.color = "black";
    this.showAndCorrectMe();
  };
};

var addToTree = function (e) {
  e.preventDefault();
  input = document.getElementById("tree-input");
  value = parseInt(input.value);
  if (value) {
    var addNode = btree.add(value);
    // console.log(values);
  } else {
    alert("Wrong input");
  }
};

var showCorrect = function (nodeSC, theBTree) {
  // console.log("before show correct", nodeSC);
  if (nodeSC) {
    // theBTree.recursiveAddNode(theBTree.root, null, null, nodeSC.data, nodeSC.color);
    showCorrectNonRecursif(nodeSC, theBTree);
    // console.log("insert of ", nodeSC);
    showCorrect(nodeSC.right, theBTree);
    showCorrect(nodeSC.left, theBTree);
  }
};

var showCorrectNonRecursif = function (nodeSCNR, theTree) {
  // console.log("Show correct non recursif of", nodeSCNR);
  var xNodeSCNR = theTree.root;
  var yNodeSCNR;
  while (xNodeSCNR != null) {
    yNodeSCNR = xNodeSCNR;
    if (nodeSCNR.data <= xNodeSCNR.data) {
      xNodeSCNR = xNodeSCNR.left;
    } else {
      xNodeSCNR = xNodeSCNR.right;
    }
  }
  // console.log("Ynode father", yNodeSCNR);

  // if (!yNodeSCNR) {
  //   theTree.root = newAddNodeSCNR;
  // } else {
  // console.log(
  //   "compare " + yNodeSCNR.data + " and " + nodeSCNR.data,
  //   yNodeSCNR.data > nodeSCNR.data
  // );
  var newAddNodeSCNR;
  if (nodeSCNR.data <= yNodeSCNR.data) {
    var leftChildCoordonate = yNodeSCNR.leftCoordinate();
    newAddNodeSCNR = theTree.addAndDisplayNode(
      leftChildCoordonate.cx,
      leftChildCoordonate.cy,
      15,
      theTree.ctx,
      nodeSCNR.data,
      nodeSCNR.color
    );
    yNodeSCNR.left = newAddNodeSCNR;
    // console.log("Add left", newAddNodeSCNR);
  } else {
    var rightChildCoordonate = yNodeSCNR.rightCoordinate();
    newAddNodeSCNR = theTree.addAndDisplayNode(
      rightChildCoordonate.cx,
      rightChildCoordonate.cy,
      15,
      theTree.ctx,
      nodeSCNR.data,
      nodeSCNR.color
    );
    yNodeSCNR.right = newAddNodeSCNR;
    // console.log("Add left", newAddNodeSCNR);
  }
  newAddNodeSCNR.parent = yNodeSCNR;

  var lineSCNR = new Line();
  lineSCNR.draw(
    yNodeSCNR.getX(),
    yNodeSCNR.getY(),
    newAddNodeSCNR.getX(),
    newAddNodeSCNR.getY(),
    15,
    theTree.ctx
  );
  // }
};

//Here is the definition of function which display in another canvas, the node in his  the array
// addFromValues = function () {
//   //creation of the canvas
//   var view = document.getElementById("view");
//   var thediv = document.createElement("div");
//   thediv.className = "row tree center";
//   var currentCanvas = document.createElement("canvas");
//   currentCanvas.width = 1200;
//   currentCanvas.height = 400;
//   thediv.appendChild(currentCanvas);
//   view.appendChild(thediv);
// };
function appendCanvas() {
  console.log("append canvas");
  areaNum++;
  var view = document.getElementById("view");
  var thediv = document.createElement("div");
  thediv.className = "row tree center";
  var currentCanvas = document.createElement("canvas");
  currentCanvas.width = 4000;
  currentCanvas.height = 1000;
  currentCanvas.id = `my-canvas-${areaNum}`;

  thediv.appendChild(currentCanvas);
  view.appendChild(thediv);
}

var btree = new BTree();

function show() {
  btree.show();
}
// var node1 = new Node(2000, 20, 15, btree.getCtx(), 1, "black");
// var node2 = new Node(2045, 65, 15, btree.getCtx(), 2, "red");
// var node3 = new Node(2090, 110, 15, btree.getCtx(), 3, "red");
// node1.parent = null;
// node1.left = node2;
// node2.left = node3;
// node2.parent = node1;
// node3.parent = node2;
// btree.root = node1;
// btree.insertCorrect(node3);

// var node1 = new Node(1910, 110, 15, btree.getCtx(), 1, "red");
// var node2 = new Node(1955, 65, 15, btree.getCtx(), 2, "red");
// console.log("Node compare", node1 === node1);
// var node3 = new Node(2000, 20, 15, btree.getCtx(), 3, "black");
// node3.parent = null;
// node3.left = node2;
// node2.left = node1;
// node2.parent = node3;
// node1.parent = node2;
// btree.root = node3;
// btree.insertCorrect(node1);

// btree.insertCorrect(node1);
// console.log("node 1 ", node1);
// console.log("node 2 ", node2);
// console.log("node 3 ", node3);
// btree.show();
// btree.leftRotate(node2);
// console.log("Root ", btree.root);
// btree.rightRotate(node2);
// console.log("node 1 ", node1);
// console.log("node 2 ", node2);
// console.log("node 3 ", node3);

// btree.showAndCorrectMe();
// btree.show();
